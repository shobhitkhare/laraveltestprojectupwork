<?php

namespace App\Http\Controllers;

use App\Product;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class ProductController extends Controller
{
    public function index()
    {
    	if(Gate::allows('manage-product', Auth::user()))
    	{
    		return view('product.index');
    	}
    	else
    	{
    		print_r('You are not authorized');
    	}
    }

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data()
	{
	    return DataTables::of(Product::query())->make(true);
	}

	public function updateStatus(Request $request)
	{
		$id = $request->id;

		$product = Product::find($id);
		if($product)
		{
			if($product->in_stock == true)
			{
				$product->update([
					'in_stock' => false
				]);
			}
			else
			{
				$product->update([
					'in_stock' => true
				]);
			}
			return response()->json(['success'=>'true','message' => 'Product status updated successfully']);
		}
		else
		{
			return response()->json(['error' => 'Something went wrong!'], 401);
		}
	}
}
