<?php

namespace App\Http\Controllers;

use App\Customer;
use App\User;
use Bouncer;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;

class CustomerController extends Controller
{
    public function index()
    {
        if (Gate::allows('manage-customer', Auth::user())) {
            return view('customer.index');
        }
    	else
    	{
    		print_r('You are not authorized');
    	}
    }

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data()
	{
	    return DataTables::of(Customer::query())->make(true);
	}
}
