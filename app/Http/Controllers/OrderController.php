<?php

namespace App\Http\Controllers;

use App\Order;
use App\User;
use Illuminate\Http\Request;
use DataTables;
use Yajra\DataTables\Html\Builder;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Spatie\Activitylog\Models\Activity;

class OrderController extends Controller
{
    public function index()
    {
    	if(Gate::allows('manage-order', Auth::user()))
    	{
    		return view('order.index');
    	}
    	else
    	{
    		print_r('You are not authorized');
    	}
    }

	/**
	 * Process datatables ajax request.
	 *
	 * @return \Illuminate\Http\JsonResponse
	 */
	public function data()
	{
		$orders = Order::with('customer')->select('orders.*');
	    return DataTables::eloquent($orders)
									->addColumn('action', function ($order) {
						                return '<a href="orders/'.$order->id.'" class="btn btn-xs btn-primary"><i class="glyphicon glyphicon-edit"></i> View</a>';
						            })
                                  ->make(true);
	}

	public function view($id)
    {
    	if(Gate::allows('manage-order', Auth::user()))
    	{
            $activities = Activity::all();
	    	$order = Order::findOrFail($id);
	    	$log = AUth::User()->name.' processed the order: '.$order->id;
	    	activity()->log($log);
	        return view('order.view',['order' => $order,'activities' => $activities]);
        }
    	else
    	{
    		print_r('You are not authorized');
    	}
    }

}
