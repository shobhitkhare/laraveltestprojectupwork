<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('manage-customer', function ($user) {
            if($user->isA('user-manager') || $user->isAn('administrator'))
            {
                return true;
            }
            else 
            {
                return false;
            }
        });
        Gate::define('manage-product', function ($user) {
            if($user->isA('shop-manager') || $user->isAn('administrator'))
            {
                return true;
            }
            else 
            {
                return false;
            }
        });
        Gate::define('manage-order', function ($user) {
            if($user->isA('shop-manager') || $user->isAn('administrator'))
            {
                return true;
            }
            else 
            {
                return false;
            }
        });
    }
}
