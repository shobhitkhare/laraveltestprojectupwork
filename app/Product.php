<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'products';

    protected $guarded = [];

    public function orderItemProducts()
    {
        return $this->hasMany('App\OrderItem', 'product_id');
    }
}
