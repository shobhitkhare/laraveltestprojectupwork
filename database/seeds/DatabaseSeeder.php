<?php

use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        \Bouncer::allow('administrator')->everything();
	    \Bouncer::allow('user-manager')->toManage(\App\Customer::class);
	    \Bouncer::allow('shop-manager')->toManage(\App\Product::class);
	    \Bouncer::allow('shop-manager')->toManage(\App\Order::class);

	    $admin = factory(App\User::class)->create();

	    $admin->assign('administrator');

	    $userManager = factory(App\User::class)->create();

	    $userManager->assign('user-manager');

	    $shopManager = factory(App\User::class)->create();

	    $shopManager->assign('shop-manager');

	    factory(App\Customer::class,150)->create();
	    factory(App\Product::class,50)->create();
    	factory(App\OrderItem::class,50)->create();
    }
}
