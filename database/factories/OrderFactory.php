<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
    	'customer_id' => function () {
            return factory(App\Customer::class)->create()->id;
        },
        'invoice_number' => $faker->randomNumber($nbDigits = 7, $strict = false),
        'total_amount' => $faker->randomFloat($nbMaxDecimals = 2, $min = 0, $max = 10000),
        'status' => $faker->randomElement(['new' ,'processed']),
    ];
});
