<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        'order_id' => function () {
            return factory(App\Order::class)->create()->id;
        },

        'product_id' => function () {
            return factory(App\Product::class)->create()->id;
        },

        'quantity' => $faker->numberBetween($min = 1, $max = 100),
    ]; 
});
