@extends('layouts.app')

@section('content')
    <div class="container-fluid">
        <div>
            <div>
                <h3>Activity Log</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Sr. No.</th>
                            <th>Log Detailse</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($activities as $activity)
                        <tr>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$activity->description}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <h3>Order #{{ $order->id }}</h3>
            <div>
                <div class="table-responsive">
                    <table class="table">
                        <tbody>
                            <tr>
                                <th>ID</th><td>{{ $order->id }}</td>
                            </tr>
                            <tr>
                                <th> Customer Name </th><td> {{ $order->customer->name }} </td>
                            </tr>
                            <tr>
                                <th> Total Amount </th><td> {{ $order->total_amount }} </td>
                            </tr>
                            <tr>
                                <th> Status </th><td> {{ $order->status }} </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
