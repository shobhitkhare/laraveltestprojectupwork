@extends('layouts.app')
@section('styles')
<style type="text/css">
    .switch {
  position: relative;
  display: inline-block;
  width: 60px;
  height: 34px;
}

.switch input { 
  opacity: 0;
  width: 0;
  height: 0;
}

.slider {
  position: absolute;
  cursor: pointer;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: #ccc;
  -webkit-transition: .4s;
  transition: .4s;
}

.slider:before {
  position: absolute;
  content: "";
  height: 26px;
  width: 26px;
  left: 4px;
  bottom: 4px;
  background-color: white;
  -webkit-transition: .4s;
  transition: .4s;
}

input:checked + .slider {
  background-color: #2196F3;
}

input:focus + .slider {
  box-shadow: 0 0 1px #2196F3;
}

input:checked + .slider:before {
  -webkit-transform: translateX(26px);
  -ms-transform: translateX(26px);
  transform: translateX(26px);
}

/* Rounded sliders */
.slider.round {
  border-radius: 34px;
}

.slider.round:before {
  border-radius: 50%;
}
</style>
@endsection
@section('content')
<div class="message">
    
</div>
<table class="table table-bordered" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Price</th>
                <th>In Stock</th>
            </tr>
        </thead>
    </table>
@endsection
{{-- <input type="checkbox" checked data-toggle="toggle" onchange="statusUpdate('+row.id+')"> --}}
@section('scripts')
<script>
$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('product.data') !!}',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'name', name: 'name' },
            { data: 'price', name: 'price' },
            { data: 'in_stock',"render": function (data, type, row) {
                          return (data == true) ? '<label class="switch"><input type="checkbox" checked onchange="statusUpdate('+row.id+')"><span class="slider round"></span></label>' : '<label class="switch"><input type="checkbox" onchange="statusUpdate('+row.id+')"><span class="slider round"></span></label>';}
           }
        ]
    });
});

function statusUpdate(id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $.ajax({
        url: "{{route('product.updateStatus')}}",
        method: 'post',
        data: {
            id: id
        },
        success: function (result) {
            var successHtml = '<div class="alert alert-success">'+
                '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong><i class="glyphicon glyphicon-ok-sign push-5-r"></</strong> '+ result.message +
                '</div>';
            $('.message').html(successHtml);
            setTimeout(function() {
                 location.reload(true);
            }, 1000);
           
        },
        error: function (xhr) {
            var successHtml = '<div class="alert alert-danger">'+
                '<button type="button" class="close" data-dismiss="alert">&times;</button>'+
                '<strong><i class="glyphicon glyphicon-ok-sign push-5-r"></</strong> Something went wrong.</div>';
            $('.message').html(successHtml);
        }
    });
};
</script>
@endsection