<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {

	Route::get('/customers', 'CustomerController@index')->name('customer.index');
	Route::get('/get-customer-data', 'CustomerController@data')->name('customer.data');

	Route::get('/products', 'ProductController@index')->name('product.index');
	Route::get('/get-product-data', 'ProductController@data')->name('product.data');
	Route::post('/update-status', 'ProductController@updateStatus')->name('product.updateStatus');

	Route::get('/orders', 'OrderController@index')->name('order.index');
	Route::get('/orders/{id}', 'OrderController@view')->name('order.view');
	Route::get('/get-order-data', 'OrderController@data')->name('order.data');
});